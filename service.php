<?php

require_once __DIR__.'/vendor/autoload.php';
require_once("funciones.php");

$server = new soap_server();

$server->configureWSDL('Programacion Computacional IV', 'urn:Prueba');

$server->wsdl->addComplexType('medicamento','complexType','struct','all','',
array(
'Codigo'=>array('Codigo'=>'Codigo','type'=>'xsd:string'),
'NombreMedicamento'=>array('NombreMedicamento'=>'NombreMedicamento','type'=>'xsd:string'),
'Descripcion'=>array('Descripcion'=>'Descripcion','type'=>'xsd:string'),
'Precio'=>array('Precio'=>'Precio','type'=>'xsd:string'),
'NombreVendedor'=>array('NombreVendedor'=>'NombreVendedor','type'=>'xsd:string')
)
);

$server->configureWSDL('Tabla 2', 'urn:Prueba');

$server->wsdl->addComplexType('empleado','complexType','struct','all','',
array(

'CodigoEmpleado'=>array('CodigoEmpleado'=>'CodigoEmpleado','type'=>'xsd:string'),
'NombreEmpleado'=>array('NombreEmpleado'=>'NombreEmpleado','type'=>'xsd:string'),
'ApellidoEmpleado'=>array('ApellidoEmpleado'=>'ApellidoEmpleado','type'=>'xsd:string'),
'Direccion'=>array('Direccion'=>'Direccion','type'=>'xsd:string'),
'sueldo'=>array('sueldo'=>'sueldo','type'=>'xsd:string')
)
);

//se define el areglo que va a contener estos structs:

$server->wsdl->addComplexType('medicamentoArray','complextype','array','','SOAP-ENC:Array',
array(),
array(
    array(
'ref'=>'SOAP-ENC:arrayType',
'wsdl:arrayType'=>'tns:medicamento[]'
    )
    ));

    $server->wsdl->addComplexType('empleadoArray','complextype','array','','SOAP-ENC:Array',
array(),
array(
    array(
'ref'=>'SOAP-ENC:arrayType',
'wsdl:arrayType'=>'tns:empleado[]'
    )
    ));

    //registro de la funcion listarmedicamento

    $server->register(
'listarmedicamento',
array('datos'=>'xsd:string'),
array('result'=>'xsd:bool','medicamento'=>'tns:medicamentoArray'),
'urn:PruebaWSDL',
'urn:PruebaWSDL#listarmedicamento'

    );

    $server->register(
        'listarempleado',
        array('datos'=>'xsd:string'),
        array('result'=>'xsd:bool','empleado'=>'tns:empleadoArray'),
        'urn:PruebaWSDL',
        'urn:PruebaWSDL#listarempleado'
        
            );

//registro de la funcion buscarmedicamento
$server->register(
'buscarmedicamento',
array('Codigo'=>'xsd:string'),
array('result'=>'xsd:bool','medicamento'=>'tns:medicamentoArray'),
'urn:PruebaWSDL',
'urn:PruebaWSDL#buscarmedicamento'


);
$server->register(
    'buscarempleado',
    array('CodigoEmpleado'=>'xsd:string'),
    array('result'=>'xsd:bool','empleado'=>'tns:empleadoArray'),
    'urn:PruebaWSDL',
    'urn:PruebaWSDL#buscarempleado'
    
    
    );

//registro de funcion nuevomedicamento
$server->register(
'nuevomedicamento',
array("Codigo"=>"xsd:string","NombreMedicamento"=>"xsd:string","Descripcion"=>"xsd:string","Precio"=>"xsd:string","NombreVendedor"=>"xsd:string"),
array('return'=>'xsd:boolean'),
'urn:PruebaWSDL',
'urn:PruebaWSDL#nuevomedicamento'

);

$server->register(
    'nuevoempleado',
    array("CodigoEmpleado"=>"xsd:string","NombreEmpleado"=>"xsd:string","ApellidoEmpleado"=>"xsd:string",
    " Direccion"=>"xsd:string","sueldo"=>"xsd:string"),
    array('return'=>'xsd:boolean'),
    'urn:PruebaWSDL',
    'urn:PruebaWSDL#nuevoempleado'
    
    );

//registro de la funcion actualizarmedicamento
$server->register(
'actualizarmedicamento',
array("Codigo"=>"xsd:string","NombreMedicamento"=>"xsd:string","Descripcion"=>"xsd:string","Precio"=>"xsd:string","NombreVendedor"=>"xsd:string"),
array('return'=>'xsd:boolean'),
'urn:PruebaWSDL',
'urn:PruebaWSDL#actualizarmedicamento'

);

$server->register(
    'actualizarempleado',
    array("CodigoEmpleado"=>"xsd:string","NombreEmpleado"=>"xsd:string","ApellidoEmpleado"=>"xsd:string",
    " Direccion"=>"xsd:string","sueldo"=>"xsd:string"),
    array('return'=>'xsd:boolean'),
    'urn:PruebaWSDL',
    'urn:PruebaWSDL#actualizarempleado'
    
    );


//registro de la funcion eliminar 
$server->register(
'eliminarmedicamentos',
array("Codigo"=>"xsd:string"),
array('return'=>'xsd:boolean'),
'urn:PruebaWSDL',
'urn:PruebaWSDL#eliminarmedicamentos'

);
$server->register(
    'eliminarempleado',
    array("CodigoEmpleado"=>"xsd:string"),
    array('return'=>'xsd:boolean'),
    'urn:PruebaWSDL',
    'urn:PruebaWSDL#eliminarempleado'
    
    );

//aki
//permite leer datos POST sin procesar
@$server->service(file_get_contents("php://input"));

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''




















